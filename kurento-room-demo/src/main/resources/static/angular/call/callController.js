/*
 * @author Micael Gallego (micael.gallego@gmail.com)
 * @author Raquel Díaz González
 */

kurento_room.controller('callController', function ($scope, $window, ServiceParticipant, ServiceRoom, Fullscreen, LxNotificationService) {

    $scope.roomName = ServiceRoom.getRoomName();
    $scope.userName = ServiceRoom.getUserName();
    $scope.participants = ServiceParticipant.getParticipants();
    $scope.kurento = ServiceRoom.getKurento();

    $scope.leaveRoom = function () {

        ServiceRoom.getKurento().close();

        ServiceParticipant.removeParticipants();

        //redirect to login
        $window.location.href = '#/login';
    };

    window.onbeforeunload = function () {
    	//not necessary if not connected
    	if (ServiceParticipant.isConnected()) {
    		ServiceRoom.getKurento().close();
    	}
    };


    $scope.goFullscreen = function () {

        if (Fullscreen.isEnabled())
            Fullscreen.cancel();
        else
            Fullscreen.all();

    };

    $scope.onOffVolume = function () {
        var localStream = ServiceRoom.getLocalStream();
        var element = document.getElementById("buttonVolume");
        if (element.classList.contains("md-volume-off")) { //on
            element.classList.remove("md-volume-off");
            element.classList.add("md-volume-up");
            localStream.audioEnabled = true;
        } else { //off
            element.classList.remove("md-volume-up");
            element.classList.add("md-volume-off");
            localStream.audioEnabled = false;

        }
    };

    $scope.onOffVideocam = function () {
        var localStream = ServiceRoom.getLocalStream();
        var element = document.getElementById("buttonVideocam");
        if (element.classList.contains("md-videocam-off")) {//on
            element.classList.remove("md-videocam-off");
            element.classList.add("md-videocam");
            localStream.videoEnabled = true;
        } else {//off
            element.classList.remove("md-videocam");
            element.classList.add("md-videocam-off");
            localStream.videoEnabled = false;
        }
    };

    $scope.disconnectStream = function() {
    	var localStream = ServiceRoom.getLocalStream();
    	var participant = ServiceParticipant.getMainParticipant();
    	if (!localStream || !participant) {
    		LxNotificationService.alert('Error!', "Not connected yet", 'Ok', function(answer) {
            });
    		return false;
    	}
    	ServiceParticipant.disconnectParticipant(participant);
    	ServiceRoom.getKurento().disconnectParticipant(participant.getStream());
    }
    
    //chat
    $scope.message;

    $scope.sendMessage = function () {
        console.log("Sending message", $scope.message);
        var kurento = ServiceRoom.getKurento();
        kurento.sendMessage($scope.roomName, $scope.userName, $scope.message);
        $scope.message = "";
    };

    //open or close chat when click in chat button
    $scope.toggleChat = function () {
        var selectedEffect = "slide";
        // most effect types need no options passed by default
        var options = {direction: "right"};
        if ($("#effect").is(':visible')) {
            $("#content").animate({width: '100%'}, 500);
        } else {
            $("#content").animate({width: '80%'}, 500);
        }
        // run the effect
        $("#effect").toggle(selectedEffect, options, 500);
    };
    
    $scope.showHat = function () {
    	var targetHat = false;
    	var offImgStyle = "md-mood";
    	var offColorStyle = "btn--deep-purple";
    	var onImgStyle = "md-face-unlock";
    	var onColorStyle = "btn--purple";
    	var element = document.getElementById("hatButton");
        if (element.classList.contains(offImgStyle)) { //off
            element.classList.remove(offImgStyle);
            element.classList.remove(offColorStyle);
            element.classList.add(onImgStyle);
            element.classList.add(onColorStyle);
            targetHat = true;
        } else if (element.classList.contains(onImgStyle)) { //on
            element.classList.remove(onImgStyle);
            element.classList.remove(onColorStyle);
            element.classList.add(offImgStyle);
            element.classList.add(offColorStyle);
            targetHat = false;
        }
    	
        var hatTo = targetHat ? "on" : "off";
    	console.log("Toggle hat to " + hatTo);
    	ServiceRoom.getKurento().sendCustomRequest({hat: targetHat}, function (error, response) {
    		if (error) {
                console.error("Unable to toggle hat " + hatTo, error);
                LxNotificationService.alert('Error!', "Unable to toggle hat " + hatTo, 
                		'Ok', function(answer) {});
        		return false;
            } else {
            	console.debug("Response on hat toggle", response);
            }
    	});
    };



    // VISCA
    var VISCA = {
        CAM_Zoom: {
            Stop: function(x) {
                return "8" + x + " 01 04 07 00 FF";
            },
            TeleStandard: function(x,p) {
                return "8" + x + " 01 04 07 2" + p + " FF";
            },
            WideStandard: function(x,p) {
                return "8" + x + " 01 04 07 3" + p + " FF";
            }
        },
        CAM_Backlight: {
            On: function(x) {
                return "8" + x + " 01 04 33 02 FF";
            },
            Off: function(x) {
                return "8" + x + " 01 04 33 03 FF";
            }
        },
        Pan_tiltDrive: {
            Up: function(x, v, w) {
                return "8" + x + " 01 06 01 " + v + " " + w +" 03 01 FF";
            },
            Down: function(x, v, w) {
                return "8" + x + " 01 06 01 " + v + " " + w +" 03 02 FF";
            },
            Left: function(x, v, w) {
                return "8" + x + " 01 06 01 " + v + " " + w +" 01 03 FF";
            },
            Right: function(x, v, w) {
                return "8" + x + " 01 06 01 " + v + " " + w +" 02 03 FF";
            },
            Stop: function(x, v, w) {
                return "8" + x + " 01 06 01 " + v + " " + w +" 03 03 FF";
            },
            Home: function(x) {
                return "8" + x + " 01 06 04 FF";
            }
        }
    };

    $scope.VISCA_Camera = {

        ZOOM_IN: function (camera) {
            jsonPost(VISCA.CAM_Zoom.TeleStandard(camera, 5));
        },
        ZOOM_OUT: function (camera) {
            jsonPost(VISCA.CAM_Zoom.WideStandard(camera, 5));
        },
        ZOOM_STOP: function (camera) {
            jsonPost(VISCA.CAM_Zoom.Stop(camera));
        },
        BACKLIGHT_ON: function (camera) {
            jsonPost(VISCA.CAM_Backlight.On(camera));
        },
        BACKLIGHT_OFF: function (camera) {
            jsonPost(VISCA.CAM_Backlight.Off(camera));
        },
        MOVE_UP: function (camera, pan_speed, tilt_speed) {
            if (camera === undefined) {
                camera = "1";
            }
            if (camera === undefined) {
                camera = "1";
            }
            if (pan_speed === undefined) {
                pan_speed = "05";
            }
            if (tilt_speed === undefined) {
                tilt_speed = "05";
            }
            jsonPost(VISCA.Pan_tiltDrive.Up(camera, pan_speed, tilt_speed));
        },
        MOVE_DOWN: function (camera, pan_speed, tilt_speed) {
            if (camera === undefined) {
                camera = "1";
            }
            if (pan_speed === undefined) {
                pan_speed = "05";
            }
            if (tilt_speed === undefined) {
                tilt_speed = "05";
            }
            jsonPost(VISCA.Pan_tiltDrive.Down(camera, pan_speed, tilt_speed));
        },
        MOVE_LEFT: function (camera, pan_speed, tilt_speed) {
            if (camera === undefined) {
                camera = "1";
            }
            if (pan_speed === undefined) {
                pan_speed = "05";
            }
            if (tilt_speed === undefined) {
                tilt_speed = "05";
            }
            console.log("Sending Visca command MOVE_LEFT");
            var kurento = ServiceRoom.getKurento();
            kurento.ptzMessage($scope.roomName, $scope.userName, 'MOVE_LEFT');
        },
        MOVE_RIGHT: function (camera, pan_speed, tilt_speed) {
            if (camera === undefined) {
                camera = "1";
            }
            if (pan_speed === undefined) {
                pan_speed = "05";
            }
            if (tilt_speed === undefined) {
                tilt_speed = "05";
            }
            jsonPost(VISCA.Pan_tiltDrive.Right(camera, pan_speed, tilt_speed));
        },
        MOVE_STOP: function (camera, pan_speed, tilt_speed) {
            if (camera === undefined) {
                camera = "1";
            }
            if (pan_speed === undefined) {
                pan_speed = "05";
            }
            if (tilt_speed === undefined) {
                tilt_speed = "05";
            }
            console.log("Sending Visca command MOVE_STOP");
            var kurento = ServiceRoom.getKurento();
            kurento.ptzMessage($scope.roomName, $scope.userName, 'MOVE_STOP');
        },
        MOVE_HOME: function (camera) {
            if (camera === undefined) {
                camera = "1";
            }
            jsonPost(VISCA.Pan_tiltDrive.Home(camera));
        },
    };



});


